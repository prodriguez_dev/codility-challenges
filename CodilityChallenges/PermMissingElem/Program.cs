﻿using System;

namespace PermMissingElem
{
    class Program
    {
        static void Main(string[] args)
        {
            Solution solution = new Solution();
            var A = new int[] { 2, 3, 1, 5 };
            var B = new int[] { };
            var C = new int[] { 2 };
            var D = new int[] { 1, 2, 3 };
            var E = new int[] { 2, 3, 4 };
            var F = new int[] { 3, 5 };
            Console.WriteLine($"Expected: 4. Result = {solution.solution(A)}");
            Console.WriteLine($"Expected: 1. Result = {solution.solution(B)}");
            Console.WriteLine($"Expected: 1. Result = {solution.solution(C)}");
            Console.WriteLine($"Expected: 4. Result = {solution.solution(D)}");
            Console.WriteLine($"Expected: 1. Result = {solution.solution(E)}");
            Console.WriteLine($"Expected: 4. Result = {solution.solution(F)}");
        }
    }
    class Solution
    {
        public int solution(int[] A)
        {
            Array.Sort(A);

            if (A.Length == 1)
            {
                if (A[0] == 1) return A[0] + 1;
                else return A[0] - 1;
            }

            for (int i = 1; i < A.Length; i++)
            {
                if (A[i] - A[i - 1] > 1) return A[i] - 1;
                if (A[i] == A[^1]) {
                    if (A[0] == 1) return A[^1] + 1;
                    else return 1;
                }
            }

            return 1; // empty list
        }
    }
}
