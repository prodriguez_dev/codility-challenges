﻿using System;

namespace CountDiv
{
    class Program
    {
        static void Main(string[] args)
        {
            Solution solution = new Solution();
            Console.WriteLine($"Expected: 3. Result: {solution.solutionA(6, 11, 2)}");
            Console.WriteLine($"Expected: ?. Result: {solution.solutionA(100, 123000, 2)}");
            //Console.WriteLine($"Expected: 3. Result: {solution.solutionA(0, int.MaxValue, 2)}"); // long running task

            Console.WriteLine($"Expected: 3. Result: {solution.solutionB(6, 11, 2)}");
            Console.WriteLine($"Expected: ?. Result: {solution.solutionB(100, 123000, 2)}");
            Console.WriteLine($"Expected: ?. Result: {solution.solutionB(0, 2000000000, 100000)}");

            Console.WriteLine($"Expected: 3. Result: {solution.solutionC(6, 11, 2)}");
            Console.WriteLine($"Expected: ?. Result: {solution.solutionC(100, 123000, 2)}");
            Console.WriteLine($"Expected: ?. Result: {solution.solutionC(0, 2000000000, 100000)}");

        }
    }

    class Solution
    {
        /// <summary>
        /// 100% correctness. 0% performance
        /// </summary>
        public int solutionA(int A, int B, int K)
        {
            if (A == B) return A % K == 0 ? 1 : 0;

            int counter = 0;

            for (int i = A; i <= B; i++)
            {
                if (i % K == 0) counter++;
            }

            return counter;
        }

        /// <summary>
        /// 100% correctness and 100% performance
        /// </summary>
        public int solutionB(int A, int B, int K)
        {
            var res = (B / K) - (A / K);

            return res + (A % K == 0 ? 1 : 0);
        }

        /// <summary>
        /// Took this from geek x geeks but it doesn't work: https://www.geeksforgeeks.org/sum-of-the-numbers-upto-n-that-are-divisible-by-2-or-5/
        /// </summary>
        public int solutionC(int A, int B, int K)
        {
            var numTerms = B / K;
            var sum = (numTerms * (2 * A + (numTerms - 1) * K)) / 2;

            return sum;
        }
    }
}
