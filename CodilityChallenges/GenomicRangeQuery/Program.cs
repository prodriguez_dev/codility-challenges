﻿using System;

namespace GenomicRangeQuery
{
    class Program
    {
        static void Main(string[] args)
        {
            Solution solution = new Solution();
            var NDA = "CAGCCTA";
            var P = new int[] { 2, 5, 0 };
            var Q = new int[] { 4, 5, 6 };
            Print(solution.solution(NDA, P, Q));
;
            var NDA2 = "C";
            var P2 = new int[] { 0 };
            var Q2 = new int[] { 0 };
            Print(solution.solution(NDA2, P2, Q2));

            Print(solution.solution("CAGCCTACAGCCTACAGCCTACAGCCTACAGCCTACAGCCTACAGCCTACAGCCTACAGCCTACAGCCTACAGCCTACAGCCTACAGCCTACAGCCTACAGCCTACAGCCTA",
                new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 15, 20, 30, 40 },
                new int[] { 2, 4, 6, 8, 10, 5, 12, 15, 20, 30, 35, 32, 42 }));

        }

        private static void Print(int[] result)
        {
            Console.Write("[");
            foreach (var item in result)
                Console.Write($"{item}, ");
            Console.Write("]");
        }
    }

    class Solution
    {
        /// <summary>
        /// 100% Correctness. 0% Performance
        /// </summary>
        public int[] solution(string S, int[] P, int[] Q)
        {
            int[] response = new int[P.Length];

            // Loop queries
            for (int i = 0; i < P.Length; i++)
            {
                int minNucleotide = 4;

                // Execute query
                for (int j = P[i]; j <= Q[i]; j++)
                {
                    int nucleotideFactor = 4;

                    switch (S[j])
                    {
                        case 'A': nucleotideFactor = 1; break;
                        case 'C': nucleotideFactor = 2; break;
                        case 'G': nucleotideFactor = 3; break;
                        default: nucleotideFactor = 4; break;
                    }

                    // keep the smallest
                    if (nucleotideFactor < minNucleotide)
                        minNucleotide = nucleotideFactor;

                    // until smallest found
                    if (minNucleotide == 1) break;
                }
                response[i] = minNucleotide;
                // return response array
            }

            return response;
        }
    }
}
