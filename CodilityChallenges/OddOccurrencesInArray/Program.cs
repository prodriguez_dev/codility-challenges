﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace OddOccurrencesInArray
{
    class Program
    {
        static void Main(string[] args)
        {
            Solution solution = new Solution();
            var result = solution.solutionA(new int[] { 9, 3, 9, 3, 9, 7, 9 });
            Console.WriteLine($"result: {result}");

            result = solution.solutionB(new int[] { 9, 3, 9, 3, 9, 9, 9 });
            Console.WriteLine($"result: {result}");            
        }
    }

    class Solution
    {
        public int solutionA(int[] A)
        {
            Dictionary<int, int> countsDictionary = new Dictionary<int, int>();

            for (int i = 0; i < A.Length; i++)
            {
                if (countsDictionary.ContainsKey(A[i]))
                    countsDictionary[A[i]]++;
                else
                    countsDictionary.Add(A[i], 1);
            }

            foreach (var item in countsDictionary)
                Console.WriteLine($"item[{item.Key}] = {item.Value}");

            return countsDictionary.First(i => i.Value == 1).Key;
        }

        public int solutionB(int[] A)
        {
            var countsDictionary = new Dictionary<int, int>();

            foreach (var item in A)
            {
                if (countsDictionary.ContainsKey(item))
                    countsDictionary[item]++;
                else
                    countsDictionary.Add(item, 1);
            }

            foreach (var item in countsDictionary)
                Console.WriteLine($"item[{item.Key}] = {item.Value}");

            return countsDictionary.First(i => i.Value % 2 != 0).Key;            
        }
    }
}
