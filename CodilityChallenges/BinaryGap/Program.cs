﻿using System;
using System.Linq;

namespace BinaryGap
{
    class Program
    {
        static void Main(string[] args)
        {
            var solution = new Solution();
            Console.WriteLine("Press any key to evaluate a number or [Esc] to exit.");

            while (Console.ReadKey().Key != ConsoleKey.Escape)
            {
                Console.Write("\nEnter a positive number: ");
                if (int.TryParse(Console.ReadLine(), out var value))
                {
                    var result = solution.LenghtOfLongestBinaryGap(value);
                    Console.WriteLine($"Length of longest binary gap: {result.ToString()}");
                }
                else Console.WriteLine($"{value} is not a number");
            }
        }
    }

    public class Solution
    {
        public int LenghtOfLongestBinaryGap(int n)
        {
            // get binary representation
            var binary = Convert.ToString(n, 2);

            Console.WriteLine($"\nBinary: {binary}");
            
            // remove leading and trailing zeros
            var trimedBinary = binary.Trim('0');

            // split by ones to get zero's vectors
            var vectors = trimedBinary.Split('1');            

            if (vectors.Length < 1) return 0;

            return vectors.Max(v => v.Length);
        }
    }
}
