﻿using System;

namespace MissingInteger
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            /* Tests
            [2]
            [1]
            [-3, -2, 1, 2]
            [-3, -2, 5, 6]
            [-3, -2, 5, 8]
             */
        }
    }

    class Solution
    {
        public int solutionA(int[] A)
        {

            if (A.Length == 1)
            {
                if (A[0] < 1) return 1;
                else if (A[0] == 1)
                    return 2;
                else return A[0] - 1;
            }
            // order array
            Array.Sort(A);

            int firstPositiveNotZero = -1;

            for (int i = 0; i < A.Length - 1; i++)
            {
                // ignore negatives - jump to first positive != 0
                if (A[i] < 1) continue;

                // get first positive not zero once
                if (firstPositiveNotZero == -1)
                { // default
                    if (A[i] > 1) return 1; // first positive missing is 1.
                    firstPositiveNotZero = A[i];
                }
                // missing element = difference greater than 1
                int diff = A[i + 1] - A[i];

                if (diff > 1)
                {
                    return A[i] + 1; // first positive missing is within the list.
                }
            }

            if (firstPositiveNotZero == -1) // all negatives
                return 1;

            // if this point is reached is because the first positive missing is at the end of the list.
            return A[A.Length - 1] + 1;

        }
    }
}
