﻿using System;
using System.Diagnostics;
using System.Linq;

namespace TapeEquilibrium
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"Expected: 1. Result: {(new Solution()).solutionA(new[] {3, 1, 2, 4, 3 })}");
            Console.WriteLine($"Expected: 2. Result: {(new Solution()).solutionA(new[] { 3, 1 })}");
            Console.WriteLine($"Expected: 1. Result: {(new Solution()).solutionA(new[] { 2, -4, 3, -2 })}");

            var mediumArray = new int[10000]; // Solution A and B will be in trouble if we increment this in one 0
            Random randomNum = new Random();

            for (int i = 0; i < mediumArray.Length; i++)
                mediumArray[i] = randomNum.Next(-100, 100);

            Stopwatch watch = new Stopwatch();
            watch.Start();
            var result = (new Solution()).solutionA(mediumArray);
            watch.Stop();
            Console.WriteLine($"Expected: ?. Result: {result}. Time: {watch.ElapsedMilliseconds}");

            watch.Restart();
            result = (new Solution()).solutionB(mediumArray);
            watch.Stop();
            Console.WriteLine($"Expected: ?. Result: {result}. Time: {watch.ElapsedMilliseconds}");

            watch.Restart();
            result = (new Solution()).solutionC(mediumArray);
            watch.Stop();
            Console.WriteLine($"Expected: ?. Result: {result}. Time: {watch.ElapsedMilliseconds}");

            Console.ReadKey();
        }
    }

    class Solution
    {
        /// <summary>
        /// Correctness: 100%. Performance: 0% (We are in the 1600 ms)
        /// </summary>
        public int solutionA(int[] A)
        {
            // find splits
            var numSplits = A.Length - 1;
            var splits = new int[numSplits];

            for (int i = 0; i < A.Length - 1; i++)
            {
                var aLength = i + 1;
                var a = new int[aLength];
                var bLength = A.Length - aLength;
                var b = new int[bLength];

                // sum each list
                Array.Copy(A, 0, a, 0, aLength);
                Array.Copy(A, aLength, b, 0, bLength);

                var aSum = a.Sum();
                var bSum = b.Sum();

                // calculate absolute diference
                splits[i] = (aSum - bSum) < 0 ? bSum - aSum : aSum - bSum;
            }

            // return mininum
            return splits.Min();
        }

        /// <summary>
        /// Correctness: 100%. Performance: %0 (We are in the 450 ms)
        /// </summary>
        public int solutionB(int[] A)
        {
            // find splits
            var numSplits = A.Length - 1;
            var splits = new int[numSplits];

            for (int i = 0; i < A.Length - 1; i++)
            {
                int sumA = 0;
                int sumB = 0;

                for (int a = 0; a <= i; a++)
                    sumA += A[a];

                for (int b = i + 1; b < A.Length; b++)
                    sumB += A[b];

                // calculate absolute diference
                splits[i] = Math.Abs(sumA - sumB);
            }

            // return mininum
            return splits.Min();
        }

        /// <summary>
        /// Correctness: 100%. Performance: 100%
        /// </summary>
        public int solutionC(int[] A)
        {
            int minDif = int.MaxValue; // Start big and update with minor difference found.
            int incSum = 0; // This will be the summarizer for the first part.
            int totalSum = A.Sum(); // We need this to calculate the second part.

            for (int i = 0; i < A.Length - 1; i++)
            {
                // first part is always incremental
                incSum += A[i];

                // second part is the total minus first part
                var restSum = totalSum - incSum;

                // calculate absolute diference
                var dif = Math.Abs(incSum - restSum);

                // if difference is less than our current min difference update it
                if (dif < minDif) minDif = dif;
            }

            // return mininum
            return minDif;
        }

    }
}
