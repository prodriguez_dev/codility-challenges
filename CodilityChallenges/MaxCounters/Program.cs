﻿using System;

namespace MaxCounters
{
    class Program
    {
        static void Main(string[] args)
        {
            Solution solution = new Solution();

            Console.Write("\n(");
            foreach (var item in solution.solutionB(5, new int[] { 3, 4, 4, 6, 1, 4, 4 })) Console.Write($"{item} ");
            Console.Write(")");
        }
    }

    class Solution
    {
        /// <summary>
        /// 100% correctness. 60% performance
        /// </summary>
        public int[] solutionA(int N, int[] A)
        {
            int[] counter = new int[N];
            int maxCounter = 0;

            // Loop A
            for (int k = 0; k < A.Length; k++)
            {
                if (A[k] >= 1 && A[k] <= N)
                {
                    counter[A[k] - 1]++;

                    // Keep Maximum value of any counter
                    if (maxCounter < counter[A[k] - 1])
                    {
                        maxCounter = counter[A[k] - 1];
                    }
                }
                else
                { // MaxCounter(): all counters set to maximum value of any counter
                    for (int j = 0; j < N; j++)
                    {
                        counter[j] = maxCounter;
                    }
                }

                Console.Write("\n(");
                foreach (var item in counter) Console.Write($"{item} ");
                Console.Write(")");
            }

            return counter;
        }

        /// <summary>
        /// 100% correctness. 100% performance
        /// </summary>
        public int[] solutionB(int N, int[] A)
        {
            int[] counter = new int[N];
            int maxCounter = 0;
            int tempMaxCounter = 0;

            // Loop A
            for (int k = 0; k < A.Length; k++)
            {
                if (A[k] >= 1 && A[k] <= N)
                {
                    if (counter[A[k] - 1] < maxCounter)
                        counter[A[k] - 1] = maxCounter;

                    counter[A[k] - 1]++;

                    // Keep temporary Maximum value of any counter
                    if (tempMaxCounter < counter[A[k] - 1])
                    {
                        tempMaxCounter = counter[A[k] - 1];
                    }
                }
                else
                {
                    // use last temp maximum as the new counter value for all counters
                    maxCounter = tempMaxCounter;
                }

                Console.Write("\n(");
                foreach (var item in counter) Console.Write($"{item} ");
                Console.Write(")");
            }

            // Loop counters and set the lastMaxCounter as the minimum for each one in case they are below that value
            for (int j = 0; j < N; j++)
            {
                if (counter[j] < maxCounter) counter[j] = maxCounter;
            }


            return counter;
        }

    }
}
