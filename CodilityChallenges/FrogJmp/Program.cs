﻿using System;

namespace FrogJmp
{
    class Program
    {
        static void Main(string[] args)
        {
            Solution solution = new Solution();
            Console.WriteLine($"X = 1, Y = 1, D = 30. Jumps: {solution.solution(1, 1, 30)}");
            Console.WriteLine($"X = 10, Y = 85, D = 30. Jumps: {solution.solution(10, 85, 30)}");
            Console.WriteLine($"X = 45, Y = 180, D = 45. Jumps: {solution.solution(45, 180, 45)}");
        }
    }
    class Solution
    {
        public int solution(int X, int Y, int D)
        {
            // write your code in C# 6.0 with .NET 4.5 (Mono)
            var distance = Y - X;

            if (distance < 1) return 0;

            if (distance < D) return 1;

            var jumps = distance / D;

            if (distance % D > 0) jumps++;

            return Convert.ToInt32(jumps);
        }
    }
}
