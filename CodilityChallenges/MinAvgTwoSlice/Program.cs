﻿using System;

namespace MinAvgTwoSlice
{
    class Program
    {
        static void Main(string[] args)
        {
            Solution solution = new Solution();
            var res =  solution.solution(new int[] { 4, 2, 2, 5, 1, 5, 8 });
            Console.WriteLine(res);
        }
    }

    class Solution
    {
        public int solution(int[] A)
        {
            var prefixSums = new float[A.Length + 1];

            // save sum totals
            for (int i = 1; i <= A.Length; i++)
            {
                prefixSums[i] = prefixSums[i - 1] + A[i - 1];
                Console.WriteLine($"prefixSums[{i}] = {prefixSums[i]}");
            }

            float minAvg = int.MaxValue;
            int startingPos = -1;

            for (int p = 0; p < A.Length - 1; p++)
            {
                for (int q = p + 1; q < A.Length; q++)
                {
                    // calculate avg
                    var avg = (prefixSums[q + 1] - prefixSums[p]) / (q - p + 1);
                    Console.WriteLine($"p:{p}, q: {q}, avg ({avg}) = prefixSums[{q+1}]({prefixSums[q+1]}) - prefixSums[{p}]({prefixSums[p]})");
                    if (avg < minAvg)
                    {
                        minAvg = avg;
                        startingPos = p;
                    }
                }
            }                 

            return startingPos;
        }
    }
}
