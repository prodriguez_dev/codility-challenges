﻿using System;

namespace PermCheck
{
    class Program
    {
        static void Main(string[] args)
        {
            Solution solution = new Solution();

            Console.WriteLine($"Expected: 1. Result: {solution.solutionA(new int[] { 4, 1, 3, 2})}");
            Console.WriteLine($"Expected: 0. Result: {solution.solutionA(new int[] { 4, 1, 3 })}");
            Console.WriteLine($"Expected: 0. Result: {solution.solutionA(new int[] { 1 })}");
            Console.WriteLine($"Expected: 0. Result: {solution.solutionA(new int[] { 1000000000 })}");
            Console.WriteLine($"Expected: 0. Result: {solution.solutionA(new int[] { 1, 1000000000 })}");
        }
    }

    class Solution
    {
        /// <summary>
        /// Correctness: 0%. Performance: 50%
        /// </summary>       
        public int solutionA(int[] A)
        {

            if (A.Length == 1) return 1; // is part of the solution but if that only element is not 1 then is not a permutation

            // Sort Array
            Array.Sort(A);

            for (int e = 1; e < A.Length; e++)
            {
                if (A[e] - A[e - 1] > 1) return 0; // is part of the solution but it doesn't take into account repeated elements
            }

            return 1;
        }

        /// <summary>
        /// Correctness: 100%. Performance: 100%
        /// </summary>       
        public int solutionB(int[] A)
        {
            // Sort Array
            Array.Sort(A);

            if (A[0] != 1) return 0; // Array does not contain 1, is not a permutation

            if (A.Length == 1) return 1; // It contains 1 and is the only element, is a permutation

            for (int e = 1; e < A.Length; e++) // it contains 1 but:
            {
                var diff = A[e] - A[e - 1];
                if (diff > 1 || diff == 0)  // > 1: has a missing element, == 0: has a repeated element
                   return 0;
            }

            return 1;
        }
    }
}
