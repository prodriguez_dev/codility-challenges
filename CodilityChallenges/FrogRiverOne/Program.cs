﻿using System;
using System.Diagnostics;

namespace FrogRiverOne
{
    class Program
    {
        static void Main(string[] args)
        {
            Solution solution = new Solution();
            Console.WriteLine($"Expected: 6. Result: {solution.solutionC(5, new[] { 1, 3, 1, 4, 2, 3, 5, 4 })}");
            Console.WriteLine($"Expected: 0. Result: {solution.solutionC(1, new[] { 1 })}");
            Console.WriteLine($"Expected: 5. Result: {solution.solutionC(6, new[] { 1, 6, 5, 2, 4, 3, 1, 5, 6, 4 })}");

            var mediumArray = new int[100000]; 
            Random randomNum = new Random();

            for (int i = 0; i < mediumArray.Length; i++)
                mediumArray[i] = randomNum.Next(1, 10000);

            Stopwatch watch = new Stopwatch();
            watch.Start();
            var result = (new Solution()).solutionC(10000, mediumArray);
            watch.Stop();
            Console.WriteLine($"Expected: ?. Result: {result}. Time: {watch.ElapsedMilliseconds}");

        }
    }

    class Solution
    {
        /// <summary>
        /// 50% correctens. 0% performance.
        /// </summary>
        public int solutionA(int X, int[] A)
        {

            int[] path = new int[X];

            for (int i = 0; i < A.Length; i++)
            {
                if (A[i] <= X)
                {
                    path[A[i] - 1] = 1;

                    // The following line is wrong because I considered that I should only evaluate the path once final position is found.
                    // Actually the final position can appear anytime and I just need to evaluate if the path has been completed once we have the
                    // right amount of positions.
                    if (A[i] == X) 
                    {
                        var pathCompleted = true;

                        // check path completed
                        foreach (var leaf in path)
                        {
                            if (leaf == 0)
                            {
                                pathCompleted = false;
                                break;
                            }
                        }

                        if (pathCompleted) return i;
                    }
                }
            }

            return -1;
        }

        /// <summary>
        /// 100% correctens. 80% performance.
        /// </summary>
        public int solutionB(int X, int[] A)
        {

            int[] path = new int[X];

            for (int i = 0; i < A.Length; i++)
            {
                if (A[i] <= X)
                {
                    // turn on leaf position. We consider one less position to fit the array length
                    path[A[i] - 1] = 1;

                    // start evaluating when we are above the amount of positions to reach the river
                    if (i >= (X - 1))
                    {
                        var pathCompleted = true;

                        // check path completed
                        foreach (var leaf in path)
                        {
                            if (leaf == 0)
                            {
                                pathCompleted = false;
                                break;
                            }
                        }

                        // earliest time
                        if (pathCompleted) return i;
                    }
                }
            }

            return -1;            
        }

        /// <summary>
        /// 100% correctens. 100% performance.
        /// </summary>
        public int solutionC(int X, int[] A)
        {

            bool[] path = new bool[X];
            int minPosAccomplished = -1;

            for (int i = 0; i < A.Length; i++)
            {
                // if the leaf is within the path
                if (A[i] <= X)
                {
                    int leafPosition = A[i] - 1; // We consider position p = A[i] - 1, one less position to fit the array length that starts in zero

                    if (path[leafPosition]) continue; // if leaf is already fallen, continue with next.
                    
                    // turn on leaf position.                     
                    path[leafPosition] = true;

                    // start evaluating when we are above the amount of positions to reach the river
                    if (i >= (X - 1))
                    {
                        var pathCompleted = true;

                        // check path completed
                        for (int p = minPosAccomplished + 1; p < X; p++)
                        {
                            if (path[p] == false)
                            {
                                minPosAccomplished = p - 1;
                                pathCompleted = false;
                                break;
                            }
                        }

                        // earliest time
                        if (pathCompleted) return i;
                    }
                }
            }

            return -1;
        }


    }

}
