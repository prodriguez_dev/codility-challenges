﻿using System;

namespace CyclicRotation
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] A = { 3, 8, 9, 7, 6};
            int K = 5;

            Print(A);

            Solution solution = new Solution();

            int[] result = solution.RotateKTimes(A, K);

            Print(result);
        }

        public static void Print(int[] A)
        {
            Console.Write("[");

            foreach (var item in A)
            {
                Console.Write($"{item}, ");
            }

            Console.Write("]");
        }
    }

    public class Solution
    {
        public int[] RotateKTimes(int[] A, int K)
        {
            int[] B = new int[A.Length];

            for (int i = 0; i < K; i++)
            {
                Array.Copy(A, B, A.Length);

                for (int j=0; j < A.Length; j++)
                {                                    
                    if (j == 0) A[j] = B[A.Length - 1];
                    else                
                    A[j] = B[j - 1];                    
                }
            }

            return A;
        }
    }
}
